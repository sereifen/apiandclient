﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //label1.Text = String.Format("Max size of file {0} Bytes", int.MaxValue);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58484/"); //LOCALHOST and the port configuret in API
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // http get tets if the api is on 
                HttpResponseMessage response = await client.GetAsync("");
                if (response.IsSuccessStatusCode)
                {
                    //get the file
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "All Files (*.*)|*.*";
                    openFileDialog.FilterIndex = 1;
                    openFileDialog.Multiselect = false;
                    if (openFileDialog.ShowDialog() != DialogResult.OK) return;
                    //get the file data
                    Stream file = openFileDialog.OpenFile();
                    var name = Path.GetFileName(openFileDialog.FileName);
                    var content = new MultipartFormDataContent(name);
                    content.Add(new StreamContent(file), openFileDialog.FileName);
                    //sent it to api
                    response = await client.PostAsync("api/upload", content);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.RequestMessage.Content.Headers.ContentType.Parameters.First();
                        //test if the name is correctly
                        if (result.Value.Contains(name)) MessageBox.Show("Upload Correctly");
                    }
                    else MessageBox.Show("file can't be upload");
                }
                
            }
        }
    }
}
